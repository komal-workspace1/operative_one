import { Component, OnInit } from '@angular/core';
import {FormBuilder,FormControl,FormGroup,FormArray} from '@angular/forms';

@Component({
  selector: 'app-nested-reactive-form',
  templateUrl: './nested-reactive-form.component.html',
  styleUrls: ['./nested-reactive-form.component.css']
})
export class NestedReactiveFormComponent implements OnInit {

  productsForm:FormGroup;
  products=[
    {'brand':'Apple'},
    {'brand':'Samsung'},
    {'brand':'Nokia'}
  ]

  constructor(
    private formBuilder:FormBuilder
  ) { }

  ngOnInit(): void {
     this.createForm(this.products);
  }

  public createForm(recievedProduts){
    var arr=[];

    recievedProduts.forEach(p => {
      arr.push(this.buildProduct(p));
    });

    this.productsForm = this.formBuilder.group({
     category:[''] ,
      brands:this.formBuilder.array(arr)

    })

  }

  showData(){
    console.log(this.productsForm.value);
  }

  buildProduct(product):FormGroup{
    return this.formBuilder.group({
      title:[product.brand],
      value:['']

    })

  }



}
