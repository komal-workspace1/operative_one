import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';
import { AuthGuard } from './auth.guard';
import { NestedReactiveFormComponent } from './nested-reactive-form/nested-reactive-form.component';


const routes: Routes = [
  {path:'',component:NestedReactiveFormComponent}
//   {path:'',pathMatch:'full',redirectTo:'login'},
// {path:'login',component:LoginComponent},
// {path:'admin',component:AdminComponent,canActivate:[AuthGuard]}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
